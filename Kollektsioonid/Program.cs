﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kollektsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = new int[] { 1, 5, 3, 8, 9, 2, 3 };
            //foreach (int arv in arvud) Console.WriteLine(arv);
            //Console.WriteLine(arvud[3]);
            arvud[3]++;

            List<int> arvud2 = new List<int>() { 8, 3, 4, 5, 6, 7, 8, 9 };
            //foreach (int arv in arvud2) Console.WriteLine(arv);
            //Console.WriteLine(arvud2[4]);
            arvud2[3]++;

            arvud2.Add(17);
            arvud2.Remove(8);
            arvud2.RemoveAt(2);

            foreach (var arv in arvud2) Console.WriteLine(arv);

            while (arvud2.Contains(8)) arvud2.Remove(8);

            List<int> arvud3 = new List<int>();
            //arvud3.Add(8);
            //arvud3.Add(3);
            //arvud3.Add(4);
            //arvud3.Add(5);
            //arvud3.Add(6); // jne

            for (int i = 0; i < 2; i++)
            {
                arvud3.Add(i * i);
                Console.WriteLine($"maht: {arvud3.Capacity} suurus: {arvud3.Count}");
            }


            arvud = arvud3.ToArray();


            List<string> nimekiriA = new List<string> { "Henn", "Ants", "Peeter" };

            int[,] tabel = { { 1, 2, 3 }, { 9, 8, 7 } };   // kahemõõtmeline
                                                           // Length = 6
                                                           // Rank() = 2 (tavalisel massiivil 1)
                                                           // GetLength(0) = 2
                                                           // GetLength(1) = 3
            int[][] suur   // massiiv mis koosneb massividest
                = new int[][] { new int[] { 1, 2, 3 }, new int[] { 9, 8, 7, 4 } };
            // suur.Length = 2
            // suur[0].Length = 3
            // suur[1].Length = 4

            List<List<int>> listideList = new List<List<int>>();
            listideList.Add(new List<int> { 1, 2, 3 });

            Dictionary<int, string> nimekiri = new Dictionary<int, string>
            {
                {1, "Henn" },
                {2, "Ants" },
                {3, "Peeter" },
            };

            if(!nimekiri.ContainsKey(3)) nimekiri.Add(3, "Joosep");
            nimekiri[2] = "Antsuke";
            nimekiri[9] = "Pilleke";
            Console.WriteLine(nimekiri[2]);

            foreach (var x in nimekiri) Console.WriteLine(x);

            Dictionary<string, double> Tulemused = new Dictionary<string, double>
            {
                {"Hiir", 1.111 },
                {"Jänes", 200 / 6.0 }
            };

            Console.WriteLine(Tulemused["Jänes"]);

            //foreach (var k in Tulemused.Keys)
            //{
            //    Tulemused[k] *= 2; // teeme tulemused 2x paremaks // ei saagi teha :(
            //    Console.WriteLine(k);
            //}

            foreach(var k in Tulemused.Keys.ToList())
            {
                Tulemused[k] *= 2; // teeme tulemused 2x paremaks // ei saagi teha :(
            }

            Console.WriteLine(Tulemused["Hiir"]);

           
        }
    }
}
