﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            int i1 = 0, i2 = 7;

            int[] arvud; // tegemist on massiiviga - aga tal pole hetkel sisu
            arvud = new int[i2]; // nüüd on massivil sisu - ta koosneb 10-st int-ist

            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = i * i;  // pöördumine indeksi kaudu
            }


            foreach (var x in arvud)
                //Console.WriteLine(x)
                ;

            for (var e = arvud.GetEnumerator(); e.MoveNext();)
            {
                var x = e.Current;
                //Console.WriteLine(x);
            }

            int[] teisedArvud = new int[10]; // muutuja defineerimine KOOS väärtusega
            int[] kolmandadArvud = (int[])teisedArvud.Clone();
            kolmandadArvud[3] = 7;
            Console.WriteLine(teisedArvud[3]);

            int[] veelYks = new int[] { 1, 3, 6, 2 }; // massiiv ja initilizer
            veelYks = new int[] { 7, 2, 3, 8, 9, 1, 0 };

            int[] viimane = { 1, 2, 3, 4, 5, 6, 78 };

            // mõned massiivid veel


            int[][] paljuarve; // mis asi see on - massiivide massiiv
            paljuarve = new int[3][];
            paljuarve[0] = new int[] { 1, 2, 3, 4 };
            paljuarve[1] = new int[] { 7, 8, 9 };
            paljuarve[2] = new int[] { 5, 6 };
            Console.WriteLine(paljuarve[1][2]); // 9

            // NB! järgmine jutt unusta ära, kui sa toast välja lähed

            int[,] tabel = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 0, 0, 10 } };
            Console.WriteLine(tabel[2,2]); // jälle 9


            object[] suvaAsjad = { 1, "Henn", 2.7, new DateTime(1955, 03, 07) };

            paljuarve[1][2]++;
            suvaAsjad[0] = (int)(suvaAsjad[0]) + 1;
        }
    }
}
