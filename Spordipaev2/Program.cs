﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Spordipaev2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Henn proovib om aoskust mööda samm sammult teha
            string filename = @"..\..\spordipäeva protokoll.txt";
            string[] sisu = File.ReadAllLines(filename);  // sisseloetud protokoll
            //Console.WriteLine(string.Join("\n", sisu));

            // ma peaks need read sisse lugema, kuhugi panema, jaotama distantside kaupa? Uhh ...
            // krt mis asja ma siin kasutaks?
            Dictionary<int, List<(string nimi, double kiirus)>> DistantsideKiirused = new Dictionary<int, List<(string, double)>>();
            // appi äkki saab nii, henn ütles, et saab tupleid kasutada, aga ta kurat ei õpetanud, kuidas, proovime ise

            for (int i = 1; i < sisu.Length; i++) // mäletate - esimese rea pidime vahele jätma
            {
                var rida = sisu[i].Replace(", ", ",").Split(','); // äge - ühe hooga splititud ka
                string nimi = rida[0];
                int distants = int.Parse(rida[1]);
                double kiirus = distants / double.Parse(rida[2]);
                // wow - mul on rea pealt nimi, kiirus ja distants
                // vaatame, kas seda distantsi on jub aolnud, kui ei ole, teeme selle distantsi nimekirja
                if (!DistantsideKiirused.ContainsKey(distants)) DistantsideKiirused.Add(distants, new List<(string, double)>());
                DistantsideKiirused[distants].Add((nimi, kiirus)); // krt asi ei saa nii lihtne olla
            }

            // huvitav, mis mul seal siis nüüd kirjas on? printida?
            foreach (var x in DistantsideKiirused)
            {
                Console.WriteLine($"distantsil {x.Key} mõõdeti {DistantsideKiirused[x.Key].Count} aega"); // mis asi see x on? mis temag ateha saaks?
                foreach (var y in x.Value) Console.WriteLine("\t" + $"({y.nimi},{y.kiirus:F2})"); // üsna paljulubav
            }

            // ma peaks nüüd iga distantsi kiireima leidma? st ma peaks uuesti tegema tsükli
            foreach(var x in DistantsideKiirused)
            {
                // x.Key on distants ja x.Value on miski list, kust on vaja leida kiireim
                int kiireim = 0; // võtame jälle esimese eelduseks
                for (int i = 1; i < x.Value.Count; i++)
                {
                    if (x.Value[i].kiirus > x.Value[kiireim].kiirus) kiireim = i;   // oi appi ma pean ju KIIRUSEID võrdlema, mitte PAARE
                }
                Console.WriteLine($"distantsil {x.Key} oli kiireim {x.Value[kiireim].nimi} tema kiirus oli {x.Value[kiireim].kiirus:F2}"); // peaagu õnnestuski

            }

            


        }
    }
}
