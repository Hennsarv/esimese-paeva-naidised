﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardipakiSegamine
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> pakk = Enumerable.Range(1, 52).ToList(); // nii saan poest segamata kaardipaki
            Random r = new Random();    // kui segada vaja, siis ikka randomit vaja

            Console.WriteLine("\npakk enne segamist\n");
            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{pakk[i]:00} {(i % 13 == 12 ? "\n" : "\t")}");
            }

            Console.WriteLine("\nesimene variant - ühest pakist teise\n");
            List<int> segatudpakk = new List<int>();  // teeme tühja paki
            while(pakk.Count > 0)
            {
                int mitmes = r.Next(pakk.Count);    // leiame juhusliku kaardi (number 0..kaartide arv)
                segatudpakk.Add(pakk[mitmes]);            // paneme ta teise pakki 
                pakk.RemoveAt(mitmes);              // kustutame ta esimesest ära
            }

            Console.WriteLine("\nsegatud pakk\n");
            for (int i = 0; i < 52; i++)
            {
                Console.Write($"{segatudpakk[i]:00} {(i % 13 == 12 ? "\n" : "\t")}");
            }




        }
    }
}
