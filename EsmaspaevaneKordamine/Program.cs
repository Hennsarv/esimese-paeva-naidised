﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeineTestKonsool
{
    class Program
    {
        static void Main(string[] args)
        {
            // seni kui t epusite, ma valmsitan mõned näited tuplitest

            (int, string) t1 = (1, "Henn");
            // t1 on "paari" tüüpi, millest üks on int ja teine string
            Console.WriteLine(t1.Item2);   // paari osad on Item1 ja Item2
            // tuplid on singlid, twinid, triplid, quadrouplid, kuni nonupliteni
            // ehk üksikud, paarid, kolmikud, nelikud kuni üheksikeni

            (int number, string nimi) t2 = (2, "Ants"); // tupli osadele saab ka nime panna
            t1 = t2; // samatüübilisi tupleid saab vastakuti väärtustada

            // tuplitel on ka muid vigureid
            t2.number++; // täitsa lubatud

            (int a, string n) = t1; // siin on tupli asemel muutjapaar
            // seda lauset tuleks lugeda a = t1.Item1, n = t1.Item2

            (_, string nn) = t1; // sama, mis eelmine, aga see a meid ei huvita

            DateTime d;
            (a, n, d) = (int.MaxValue / 2, "henn sarv".ToUpper(), DateTime.Today);
            // tupli moodi saab kolm omistamist panna ühte lausesse 
            // (mugav, kui tahad {} ära jätta) - hiljem näeme, et vahel vägagi vajalik

            // vaatame seda tupliga tehtud funktsiooni (jahh homme õpime, mis on funktsioonid)


            int[] arvud = { 17, 23, 40, 80 };
            Console.WriteLine(Arvuta(arvud).keskmine); // ma küsin funktsioonilt vaid ühte tulemust

            int s; double k;
            (_, s, k) = Arvuta(arvud);
            Console.WriteLine($"summa on {s} ja keskmine {k}");
            var csk = Arvuta(arvud); // csk on nüüd triple tüüpi
            Console.WriteLine($"summa on {csk.summa} ja keskmine {csk.keskmine}");





        }

        // üks näide funktsioonis tuplitega, kuigi me pole neid veel õppinud
        static (int mitu, int summa, double keskmine) Arvuta(IEnumerable<int> mass)
        {
            int s = 0;
            int c = 0;
            foreach (var x in mass) { s += x; c++; }
            return (c, s, (double)s / c);
            // selline funktsioon arvutab NII summa, keskmise kui loeb kokku
        }

    }
}
