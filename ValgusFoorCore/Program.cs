﻿using System;

namespace ValgusFoorCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Mis värv? ");

            Console.WriteLine(Console.ReadLine().ToLower() switch
            {
                "green" => "sõida edasi",
                "roheline" => "sõida edasi",
                "red" => "jää seisma",
                "punane" => "jää seisma",
                "yellow" => "oota rohelist",
                "kollane" => "wait the green",
                _ => "pese silmad puhtaks"
            });

        }
    }
}
