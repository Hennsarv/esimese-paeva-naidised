﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LausePlokid
{
    class Program
    {
        static bool ÄmmOnKodus = false;

        static void Main(string[] args)
        {
            // eile oli juttu natuke C# programmi ülesehitusest
            // protseduur - või algoritm või tegevusjuhend koosneb lausetest
            // kolm (peamist) lauset on
            // 1. (muutuja) definitsioon;
            // 2. (muutujale uut väärtust) arvutav avaldis;
            // 3. meetodi väljakutse (ConsoleWriteLine näiteks)

            int age = 64;
            {
                // see siin on ka lausete plokk
                string name = "Henn";
            }


            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)  // tingimuslik lause
            {
                Console.WriteLine("täna läheme sauna");
                int vihtadeArv = 7;
                Console.WriteLine($"võtame kaasa {vihtadeArv} vihta");
                if (vihtadeArv == 0) Console.WriteLine("täna ei vihtle"); // see on äärmuslikul juhul lubatud
            }
            else
            {
                // need laused, siin plokis täidetakse, kui see ifi tagune asi on false
            }

            // teeme ühe näite-variandi veel

            if(DateTime.Now.DayOfWeek == DayOfWeek.Saturday) 
            {
                // laupäevased laused
                
            }
            else if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            {
                // pühapäevased laused
            }
            else
            {
                // muud päevad
            }


            // muutuja skoop
            string see = "see on protseduuri tasemel muutuja";
            {
                int kohalik = 5;
            }

            {
                string kohalik = "khaeksa";

            }
            
            // me oleme esimese bloki ifiga jõudnud nüüd ühele poole
            // if(is-lause-on-selge) LähemeKohviJooma();




        }
    }
}
