﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tsyklid
{
    class Program
    {
        static void Main(string[] args)
        {
            // tsüklid - kui midagi on vaja teha mitu korda

            int i = 0;
            for (; ; )
            {
                Console.WriteLine($"teen seda asja {++i}-{(i == 1 || i == 2 ? "st" : "ndat") } korda");
                if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday) break;
            }

            int vastus = 0;
            for (
                    Console.Write("mis su palk on: ");  
                    ! (int.TryParse(Console.ReadLine(), out vastus)); 
                    Console.WriteLine("kirjuta ilusti\nmis su palk on: ")
                )
            { }
            Console.WriteLine($"selge, su palk on {vastus}");

            for (int j = 0; j < 10; j++)
            {

            }

            Console.WriteLine("\nwhile tsükliga\n");
            // while tsükkel
            while (DateTime.Now.DayOfWeek != DayOfWeek.Thursday)
            // for (;DateTime.Now.DayOfWeek != DayOfWeek.Friday;)
                {
                    Console.WriteLine("ootan reedet");
                System.Threading.Thread.Sleep(1000);
            }

            Console.WriteLine("\ndo tsükliga\n");
            // do-tsükkel
            do
            {
                Console.WriteLine("ootan reedet");
                System.Threading.Thread.Sleep(1000);
            } while (DateTime.Now.DayOfWeek != DayOfWeek.Thursday);

            //for (bool b = true; b; b = DateTime.Now.DayOfWeek != DayOfWeek.Thursday) 
            //{
                
            //}

        }
    }
}
