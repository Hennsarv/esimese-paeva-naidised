﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TainedavadTykid
{
    class Program
    {
        static void Main(string[] args)
        {

            #region alguses näiteks
            //string teineFile = @"..\..\Andmed.txt";
            //string sisu = File.ReadAllText(teineFile);
            //Console.WriteLine(sisu);

            //string[] loetudRead = File.ReadAllLines(teineFile);
            //for (int i = 0; i < loetudRead.Length; i++)
            //{
            //    Console.WriteLine($"rida {i} - {loetudRead[i]}");
            //}

            //int[] arvud = { 1, 7, 2, 8, 9 };
            //Console.WriteLine(string.Join(", ", arvud));

            //var nimed = File.ReadAllLines("..\\..\\Nimekiri.txt");
            //Console.WriteLine("{\"" + string.Join("\", \"",nimed) + "\"}");

            //foreach (string x in nimed) Console.WriteLine(x.Split(' ')[0]); 
            #endregion

            string filename = "..\\..\\spordipäeva protokoll.txt";
            // Console.WriteLine(File.ReadAllText(filename)); // kontrollin kas saab lugeda - hiljem kustutan

            string[] read = File.ReadAllLines(filename); // tulemuseks massiivi ridadega asi
            
            string[] nimed = new string[read.Length - 1]; // mis kuradi - 1?
            double[] kiirused = new double[read.Length - 1];

            int kiireim = 0; // mitmes on kõige kiirem
            for (int i = 0; i < nimed.Length; i++)
            {
                string[] tykid = read[i + 1].Replace(", ", ",").Split(','); // kaval - ma polegi nii varem teinud
                nimed[i] = tykid[0];
                kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]);
                Console.WriteLine($"{nimed[i]} kiirus {kiirused[i]:F2}"); // hää oli vahepeal vaadata, kas sain hakkama
                
                // kas praeguse rea kiirus on SUUREM kui meelde jäetud kiirema rea kiirus
                //     kiirused[i]                       kiirused[kiireim]
                if (kiirused[i] > kiirused[kiireim]) kiireim = i;
            }

            Console.WriteLine($"\nkõikse kiirem on {nimed[kiireim]} oma kiirusega {kiirused[kiireim]:F2}");
            // WOW! isegi toimis


        }
    }
}
