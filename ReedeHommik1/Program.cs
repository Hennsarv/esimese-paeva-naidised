﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeHommik1
{
    class Program
    {
        static void Main(string[] args)
        {
            // üks väike abiline, mida järgmistes ülesannetes vaja läheb

            Random r = new Random();  // see on juhusliku arvu genereerija andmetüüp
                                      // sellel on paar hea asja

            #region proovimiseks
            // NB! kui selle randomi "tegemisel" anda ette mingi number, siis need juhulikud
            // asjad on iga kord juhuslikult samad

            int proov = r.Next();         // rändomi käest saaab "küsida" juhullikke täisarve
            //Console.WriteLine(proov);

            proov = r.Next(100);          // juhulikke täisarve KUNI SAJANI (excl)
            //Console.WriteLine(proov);

            proov = r.Next(10, 20);        // juhulikke arve 10-st (incl) 20ni (excl)
            //Console.WriteLine(proov);

            double d = r.NextDouble();  // juhulikke double-id vahemikus 0 kuni 1 (excl)
                                        //Console.WriteLine(d); 
            #endregion

            int[] arvud = new int[80];
            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = r.Next(1000);
            }

            for (int i = 0; i < arvud.Length; i++)
            {
                /*
                Console.Write(arvud[i]);
                Console.Write("\t");
                if (i % 10 == 9) Console.WriteLine();
                // */
                Console.Write($"{arvud[i]}{(i % 10 == 9 ? "\n" : "\t")}");
            }


            // kuidas leida kõige suurem ja kõige väiksem
            int suurim = arvud[0];
            int väikseim = arvud[0];
            double summa = 0;
            for (int i = 1; i < arvud.Length; i++)
            {
                suurim = arvud[i] > suurim ? arvud[i] : suurim;
                väikseim = arvud[i] < väikseim ? arvud[i] : väikseim;
                summa += arvud[i];
            }

            Console.WriteLine("suurim on: " + suurim);
            // see on tavaline stringi liitmine - lihtne arusaada, ebamugav kirjutada ja lugeda
            // stringi liitmise tehe ei ole alati kõige viisakam
            Console.WriteLine("väikseim on: {0}", väikseim);
            // siin kasutame placeholderiga {0}, {1} jne stringi, kuhu StringFormat lisatb õigesse
            // kohta vahele prinditavad (või kuvatavad, vms) väärtused
            // pisut parem ja suht univeraalne
            Console.WriteLine($"keskmine on: {summa / arvud.Length}");
            // siin kasutame interpoleeritud stringi - $-stringi
            // kus placeholderite SISSE kirjutatakse väärtus, mida kuvada-printida

            // SIIT HAKKAB UUS HARJUTUS
            Console.WriteLine("\nHarjutus 2\n");

            int variant = r.Next(); // teen uue "korratava" randomi
            Random uusR = new Random(variant);

            int[,] table = new int[10, 10];
            for (int i = 0; i < table.GetLength(0); i++)
            {
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    table[i, j] = uusR.Next(100);
                    Console.Write(table[i, j] + "\t");
                }
                Console.WriteLine();
            }

            // variant massivide massiviga

            #region massiivide massiivi varijant
            uusR = new Random(variant); // et juhuslikud asjad algaksid samast kohast, kus eelmisel tabelil
            Console.WriteLine("\ntabeli variant [][]\n");
            int[][] tabel = new int[10][];
            for (int i = 0; i < tabel.Length; i++)
            {
                tabel[i] = new int[10];
                for (int j = 0; j < tabel[i].Length; j++)
                {
                    tabel[i][j] = uusR.Next(100);
                    Console.Write(tabel[i][j] + "\t");
                }
                Console.WriteLine();
            }

            #endregion
            
            int otsitav = -1;
            do
            {
                Console.Write("mida otsime (0..100) ");
                if (int.TryParse(Console.ReadLine(), out otsitav)
                    && otsitav >= 0
                    && otsitav < 100
                    ) break;
                Console.WriteLine("kas pole õige suurusega või pole arv");
            } while (true);
            Console.WriteLine($"otsime {otsitav}");

            bool leidsime = false;
            for (int i = 0; i < table.GetLength(0); i++)
                for (int j = 0; j < table.GetLength(1); j++)
                if (table[i,j] == otsitav)
                    {
                        leidsime = true;
                        Console.WriteLine($"leidsin {otsitav} reast {i+1} ja veerust {j+1}");
                    }
            if (!leidsime) Console.WriteLine($"arvu {otsitav} meie tabelis ei ole");
        }
    }
}
