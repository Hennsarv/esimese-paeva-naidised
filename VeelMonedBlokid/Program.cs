﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelMonedBlokid
{
    class Program
    {
        static void Main(string[] args)
        {
            // if peaks olema selge
            // switch - pisut keerulisem, aga mugavam

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    // laused, mida täidetakse laupäeval
                    Console.WriteLine("Täna saab sauna");
                    //break;
                    goto case DayOfWeek.Sunday;
                case DayOfWeek.Sunday:
                    // laused mida täidetakse pühapäeval
                    Console.WriteLine("joome õlutit");
                    break;

                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    // laused, mida täidetakse kolmapäeval
                    Console.WriteLine("lähen sulgpalli");
                    goto default;
                default:
                    // laused, mida täidetakse muudel päevadel
                    Console.WriteLine("tööle-tööle kurekesed");
                    break;

            }
            Console.Write("Press Hani Kii ...");
            Console.ReadKey();
        }
    }
}
