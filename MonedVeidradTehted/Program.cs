﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MõnedVeidradTehted
{
    class Program
    {
        static void Main(string[] args)
        {
            // paar tehet, mis on harjumatud ja millega tuleb harjuda

            // loogikatehe ja && ja või || - kirjutatakse kahekordselt

            // mida teevad tehted & ja |

            Console.WriteLine(5 & 6); // mis trükitakse - 4
            Console.WriteLine(5 | 6); // mis trükitakse - 7
            Console.WriteLine(5 ^ 6); // mis trükitakse - 3

            // kudias arvuti sismeliselt arvudest aru saab
            // 6 - 0110   6 & 5 0100 - 4
            // 5 - 0101   6 | 5 0111 - 7
            // bitwise logical operations
            // ^ bitwise xor


            string nimi = "Henn Sarv";
            // Console.WriteLine(nimi[1]+0);

            string failinimi = @"C:\Users\sarvi\OneDrive\ExtendedSpace\ValiITSalvestused\Salvestused veebruar 2020";
            string jutt1 = "Luts jutustab: \"kui arno jne jne\"";  // jutumärk escapetud stringis
            string jutt2 = @"Luts jutustab: ""kui arno jne jne"""; // jutumärk esceipimata stringis dopelt

            Console.WriteLine("Henn on ilus poiss\nja tark kahh"); // line feed ehk LF ehk char(10)
            Console.WriteLine("Henn on ilus poiss\rAnts");         // carriage return ehk CR ehk char(13)

            // string on kahtlane loom - tema on MUUTUMATU
            string nimi2 = "Henn";
            nimi2 += " ";
            nimi2 += "sarv";
            Console.WriteLine(nimi);
            Console.WriteLine(nimi2);

            Console.WriteLine(nimi == nimi2 ? "samad" : "erinevad");        // javas erinevad
            Console.WriteLine(nimi.Equals(nimi2, StringComparison.InvariantCultureIgnoreCase) ? "samad" : "erinevad");   // ka javas samad

            Console.Write("Tere ");
            Console.WriteLine("Henn");

            int arv1 = 7;
            decimal arv2 = 10.777M;

            // placeholderid stringis (stringFormat)
            Console.WriteLine("arvud on sellised {0} ja {1:F2}", arv1, arv2);
            string tulemus;
            tulemus = string.Format("arvud on sellised {0} ja {1:F2}", arv1, arv2);
            Console.WriteLine(tulemus);

            // placeholderid-avaldised - interpoleeritud string
            Console.WriteLine($"arvud on sellised {arv1} ja {arv2:F2}");
            tulemus = $"arvud on sellised {arv1} ja {arv2:F2}";
            Console.WriteLine(tulemus);

        }
    }
}
