﻿using System;

// mis asi see on - see on reakommentaar, roheline (ignoreeritav) on kõik kuni rea lõpuni

    /* või see - see on ploki kommentaa5r 
     kommentaar on kõik kuni ploki lõpuni
     
     */ 
namespace EsimeneKonsool
{
 
    class Program
    {
        

        static void Main(string[] args)
        {
            Console.WriteLine("Tere Henn!");

            //* - see on liitkommentaari kasutamise näide 
            Console.WriteLine("Täna on ilus ilm");
            //*/ Console.WriteLine("Täna on kole ilm");

            // esimene ja põhiline mõiste - muutuja
            // muutuja on nimeline mälupiirkond

            int arv = 7; // muutuja defineerimine ja väärtustamine
            int teinearv; // muutuja defineerimine (ilma väärtustamata) - halb komme
            teinearv = arv * 10; // muutuja väärtustamine (avaldisega)
            Console.WriteLine(teinearv); // muutja kasutamine (väljanäitamiseks)

            // teine fundametaalmõiste - andmetüüp
            Int32 // andmetüüp - mis asi see on
            kolmasArv // muutja nimi
            = 100 // algväärtus(avaldis)
            ; // lauset lõpetav semikoolon

            // andmetüüp - defineerib nelja asjandust
            // 1. Kui palju bitte(baite) vaja on - maht
            // 2. Mida need bitid(baidid) tähendavad - semantika
            // 3. Väärtuste hulk - süntaks, domain
            // 4. Mida nendega teha saab - operatsioonid, tehted, jms

            Console.WriteLine(kolmasArv.GetType().FullName);

            decimal d1 = 10;
            Decimal d2 = d1;

            DateTime täna = DateTime.Today;
            Console.WriteLine(täna);

            char a = 'A';
            Console.WriteLine(++a);
            Console.WriteLine('a' - 'A');

            // järgmine kole ja fundamentaalne asi on avaldis

            // avaldis on lausend, mis on pandud kokku
            // literaalidest - arvud ja muud "konstandid"
            // muutujatest
            // tehetest
            // sulgudest
            // funktsioonidest

            Console.WriteLine((arv = teinearv) * 7);

            Console.WriteLine(10 / 7);

            Console.Write("Palju sa palka saad: ");
            decimal palk = decimal.Parse(Console.ReadLine());
            string tulemus = palk < 1000 ? "vaene" : "rikas";
            string kuhu =
                palk > 1000 ? "õhtust sööma" :
                palk > 100 ? "kinno" :
                palk > 10 ? "burksi" :
                "mitte kuhugi";
            Console.WriteLine("sinuga läheme " + kuhu);

            kolmasArv += (arv += 7) * 2;
            Console.WriteLine((arv = kolmasArv) + (teinearv * arv));


            arv = 20;
            Console.WriteLine(arv++); // 20
            Console.WriteLine(++arv); // 22
            


        }



    }
}
