﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Valgusfoor1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("kas if (i) või switch(s) või elvis (e): ");
            switch (Console.ReadKey().KeyChar)
            {
                case 'i': case 'I':

                    // siia tuleb ifiga variant
                    #region if-variant

                    string värv;

                    do
                    {
                        Console.Write("\nmis värvi sa seal näed: ");
                        värv = Console.ReadLine().ToLower();
                        if (värv == "red" || värv == "punane")
                        {
                            Console.WriteLine("jää seisma");
                        }
                        else if (värv == "green" || värv == "roheline")
                        {
                            Console.WriteLine("sõida edasi");
                        }
                        else if (värv == "kollane" || värv == "yellow")
                        {
                            Console.WriteLine("oota rohelist");
                        }
                        else
                        {
                            Console.WriteLine("pese silmad puhtaks");
                        }

                    } while (värv != "green" && värv != "roheline");                   
                    
                    #endregion
                    break;
                case 's':case 'S':
                    // siia tuleb switchiga variant
                    #region switch-variant
                    bool oota = true;

                    do
                    {
                        Console.Write("\nmis värvi sa seal näed: ");
                        switch (Console.ReadLine().ToLower())
                        {
                            case "roheline":
                            case "green":
                                Console.WriteLine("sõida edasi");
                                oota = false;
                                break;
                            case "red":
                            case "punane":
                                Console.WriteLine("jää seisma");
                                break;
                            case "kollane":
                            case "yellow":
                                Console.WriteLine("oota rohelist");
                                break;
                            default:
                                Console.WriteLine("pese silmad");
                                break;
                        }

                    } while (oota);

                    #endregion
                    break;

                case 'e':
                case 'E':
                    // teeme ka elvisega variandi
                    bool edasi = false;
                    do
                    {
                        Console.Write("\nmis värvi sa seal näed: ");
                        
                        string värve = Console.ReadLine().ToLower();
                        Console.WriteLine(
                            (edasi = (värve == "green")) || (edasi = (värve == "roheline")) ? "sõida edasi" :
                            värve == "red" || värve == "punane" ? "sõida edasi" :
                            värve == "yellow" || värve == "kollane" ? "sõida edasi" :
                            "pese silmad"

                            ); 
                    } while (!edasi);

                    break;
                default:
                    Console.WriteLine("\nno ma siis ei tee");
                    break;
            }

           


        }
    }
}
