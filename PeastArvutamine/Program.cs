﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeastArvutamine
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                Random r = new Random();
                int punkte = 50;
                for (int i = 0; i < 10; i++) // 10 korda
                {
                    int vastus;
                    string tehe;
                    // kaks arvu ja tehe
                    int yks = r.Next(1, 20);
                    int kaks = r.Next(1, 20);
                    int t = r.Next(3);
                    (vastus, tehe) =                       // ei pea nii ilusti kirjutama, võib normaalse inimese kombel
                        t == 0 ? (yks + kaks, "summa") :
                        t == 1 ? (yks - kaks, "vahe") :
                        t == 2 ? (yks * kaks, "korrutis") :
                        t == 3 ? (yks / kaks, "jagatis") :
                        (0, "");
                    Console.Write($"leia arvude {yks} ja {kaks} {tehe}:  ");
                    for (int j = 0; j < 5; j++)
                    {
                        if (int.Parse(Console.ReadLine()) == vastus) break;
                        Console.Write($"vale vastus{(j == 4 ? "\n" : " - proovi uuesti: ")}");
                        punkte--;  // vale vastuse puhul võetakse 1 punkt maha
                    }
                }
                Console.WriteLine(
                    punkte == 50 ? "suurepärane - kõik õiged vastused" :
                    punkte > 40 ? "üsna tubli tulemus" :
                    punkte > 30 ? "päris hea tulemus" :
                    punkte > 10 ? "kehvake - harjuta veel" :
                    "mine algkooli tagasi"

                    );

            }
        }
    }
}
