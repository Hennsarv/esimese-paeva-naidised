﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpordiPaevMassiviga
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = @"..\..\spordipäeva protokoll.txt";
            string[] sisu = File.ReadAllLines(filename);  // sisseloetud protokoll
            //Console.WriteLine(string.Join("\n", sisu));
            int distantse = 0;
            int[] distantsid = new int[sisu.Length];
            for (int i = 1; i < sisu.Length; i++)
            {
                int distants = int.Parse(sisu[i].Split(',')[1]);
                if (!distantsid.Contains(distants)) distantsid[distantse++] = distants;  
            }



            for (int i = 0; i < distantse; i++)
            {
                Console.WriteLine(distantsid[i]);
                double suurimkiirus = 0;
                string kiireimjooksja = "";
                for (int j = 1; j < sisu.Length; j++)
                {
                    var rida = sisu[j].Split(',');
                    int distants = int.Parse(rida[1]);
                    string nimi = rida[0];
                    double kiirus = distants / double.Parse(rida[2]);
                    if (distants == distantsid[i])
                    {
                        if (suurimkiirus == 0 || kiirus > suurimkiirus ) { suurimkiirus = kiirus; kiireimjooksja = nimi; }
                    }
                    
                }
                Console.WriteLine($"disntasil {distantsid[i]} oli kiireim {kiireimjooksja}");

            }


        }
    }
}
