﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Teisendused
{
    class Program
    {
        static void Main(string[] args)
        {
            // aegajalt on vaja andmetüüpe ühest teiseks teisendada
            // või ka tüübi enda sees muuta

            // kõigepealt - mis asi on var


            #region peitsin ära
            //var ilusMuutuja = 7D; // muutuja ilusMuutuja on mul selleks, et teda uurida
            //Console.WriteLine(ilusMuutuja.GetType().Name);
            //Console.WriteLine("ilusMuutuja väärtus on {0}", ilusMuutuja);
            //var teine = ilusMuutuja;

            //var henn = new { Nimi = "Henn", Vanus = 64 };
            //Console.WriteLine(henn.GetType().Name);

            //foreach (var x in henn.GetType().GetProperties()) Console.WriteLine(x.Name);

            #endregion

            // arvusid saaab teisteks arvudeks teisendada lihtsalt
            int i = 1000;
            long l = i; // toimub ilmutamata tüübiteisendus

            i = (int)(l + l*2); // ilmutatud tüübiteisendus e casting 
            // casting on suhteliselt kõrge prioriteediga, seepärast avaldis sulgudesse

            //l = long.MaxValue; // suurim long arv
            //i = (int)l;
            //Console.WriteLine(i);

            i = int.MaxValue;
            i++;
            Console.WriteLine(i);

            double d = 10.0;
            i = (int)d;

            // samaliigilisi arve saab teisendad ailmutamata väiksemast suuremasse
            // suuremast väiksemasse või liikide vahel tuleb castida (ilmutatult teisendada)

            d = i; // integeri saab omistada kõuigile
            decimal dets = i;
            d = (float)dets;

            string tekst = "100";
            // i = (int)tekst; // see ei toimi - stringi ei saa castida teistesse tüüpidesse
            // tekst = d; // see ka ei toimi

            tekst = d.ToString(); // kõigil asjadel on olemas viis, kuidas neid stringiks tehakse
            Console.WriteLine(tekst);

            i = int.Parse("10");  // see teisendab teksti int asjaks
            Console.WriteLine(i * 2);
            DateTime dt = DateTime.Parse("March 7 1955", new CultureInfo("en-us"));
            Console.WriteLine(dt);

            d = double.Parse("14.0", new CultureInfo("en-us"));

            // tryparse võimaldab proovida ja otsustada

            Console.Write("Millal oled sa sündinud: ");
            string sünniaeg = Console.ReadLine(); // 
            //DateTime sünnipäev = DateTime.Parse(sünniaeg);
            //Console.WriteLine($"sa siis oled süninud sellisel päeval {sünnipäev.DayOfWeek}");

            //if(DateTime.TryParse(sünniaeg, out DateTime sündinud))
            //{
            //    Console.WriteLine("sinu sünnipäev on siis " + sündinud.ToShortDateString());
            //}
            //else Console.WriteLine("see pole miski sünniaeg");

            Console.WriteLine(
                DateTime.TryParse(sünniaeg, out DateTime x) ? $"sa oled siis sündinud {x}" :
                "see pole miski kuupäev"
                );

            // kolmas viis teisendada on kasutada staatilist klassi Convert kus on SUUR HULK teisendamisfunktsioone
            int teine = Convert.ToInt32("100");
            decimal teineD = Convert.ToDecimal("100,5");
            // jne jne

            // convert klassis olevad funktsioonid võimaldavad teisendada millest iganes, millesse iganes
            // kui see on võimalik

            // OLE HEA ja ÄRA KASUTA Convert klassi teisendusfunktsioone, kui suudad, sest
            // 1. need on tehtud "kehvemate" keelte harjumuste tarbeks (nagu VB-s on ToInt ja sihukesed)
            // 2. neil ei ole seda TryParse moodi asjandust
            // 3. casting ja parse on "standard"-viisid asju teisendada ja seepärast pigem kasuta neid







        }
    }
}
